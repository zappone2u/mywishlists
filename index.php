<?php

require_once 'vendor/autoload.php';


use mywishlists\controler\ControlerParticipant;
use mywishlists\controler\ControlerCreateur;
use mywishlists\controler\ControlerAuthentification;
use mywishlists\vue\VueAuthentification;
use Illuminate\Database\Capsule\Manager as DB;

$app = new \Slim\Slim();

$config = parse_ini_file('src/conf/conf.ini');
$db = new DB();
$db->addConnection( $config );
$db->setAsGlobal();
$db->bootEloquent();

$app->get("/", function () {
    $v = new VueAuthentification(VueAuthentification::SIGN_UP);
    echo $v->accueil();
})->name("Accueil");

/******* PARTIE AUTHENTIFICATION *******/

//Formulaire d'inscription
$app->get("/auth/formCreateAccount", function () use ($app) {
    $v = new VueAuthentification(VueAuthentification::SIGN_UP);
    echo $v->render();
})->name("formCreateUser");

//Formulaire de connexion
$app->get("/auth/formLoginUser", function () use ($app) {
    $v = new VueAuthentification(VueAuthentification::LOGIN);
    echo $v->render();
})->name("formLoginUser");


// A vérifier
$app->get("/auth/seConnecter", function () use ($app) {
    $v = new VueAuthentification(VueAuthentification::SIGN_UP);
    echo $v->connexion();
})->name("seConnecter");

//Formulaire de connexion avec affichage d'une erreur
$app->get("/auth/formLoginUser/er", function () use ($app) {
    $v = new VueAuthentification(VueAuthentification::BAD_LOGIN);
    echo $v->render();
})->name('formLoginUserError');

//Formulaire d'inscription lorsqu'il y a une erreur
$app->get('/auth/formCreateAccount/er', function () use ($app) {
    $v = new VueAuthentification(VueAuthentification::BAD_SIGNUP);
    echo $v->render();
})->name("formCreateAccountError");

//Création du compte
$app->post("/auth/createAccount", function () use ($app) {
    //Si la création s'est bien passé on accède directement au formulaire de connexion
    if (ControlerAuthentification::createUser())
        $app->redirect($app->urlFor("formLoginUser"));
    //Sinon, on est renvoyé sur la page du formulaire d'inscription avec un message d'erreur
    else
        $app->redirect($app->urlFor('formCreateAccountError'));
})->name("createUser");

//Connexion au compte
$app->post("/auth/signin", function () use ($app) {
    //Redirection vers la vue des listes créateurs si l'authentification a réussi
    if (ControlerAuthentification::authenticate($_POST['email'], $_POST['Password']))
        $app->redirect($app->urlFor('viewListes'));
    else //Sinon on redirige vers le formulaire d'authentification
        $app->redirect($app->urlFor('formLoginUserError'));
})->name("loginUser");

//Déconnexion au compte
$app->get("/auth/logout", function () {
    ControlerAuthentification::logout();
})->name('logout');


/*******PARTIE CREATEUR *******/

//Vue des listes du créateur
$app->get("/creator/viewLists", function () {
    $c = new ControlerCreateur();
    $c->recupererListes();
})->name("viewListes");

//Vue d'une liste côté créateur
$app->get("/creator/viewList/:idListe", function ($idListe) use ($app) {
    $c = new  ControlerCreateur();
    $c->recupererContenuListe($idListe);
})->name("viewOneListe");

//Affichage d'un item côté créateur
$app->get("/creator/viewItem/:idListe/:idItem", function ($idListe, $idItem) use ($app) {
    $c = new ControlerCreateur();
    $c->recupererItem($idListe, $idItem);
})->name('ViewItemCrea');

//Création d'un Item
$app->post("/creator/additem/:idListe", function ($idListe) use ($app) {
    $c = new ControlerCreateur();
    $c->ajouterItemDansListe($idListe);
})->name("addItem");

//Création d'une liste
$app->post("/creator/addList", function () use ($app) {
    $c = new ControlerCreateur();
    $c->creerListe();
})->name('addList');

//Formulaire de modification de liste
$app->get("/creator/setList/:idListe", function ($idListe) use ($app) {
    $c = new ControlerCreateur();
    $c->modificationListe($idListe);
})->name("formModifListe");

//Modification de la liste
$app->post("/creator/modifListe/:idListe", function ($idListe) use ($app) {
    $c = new ControlerCreateur();
    $c->setListe($idListe);
})->name('modifListe');

//Suppression d'une liste
$app->get("/creator/deleteListe/:idListe", function ($idListe) use ($app) {
    $c = new ControlerCreateur();
    $c->deleteListe($idListe);
})->name('deleteListe');

//Génération du lien de création de la liste
$app->get("/creator/shareList/:idListe", function ($idListe) use ($app) {
    $c = new ControlerCreateur();
    $c->partagerListe($idListe);
})->name('shareList');

//Modification d'un item
$app->post('/creator/setItem/:idListe/:idItem', function ($idListe, $idItem) use ($app) {
    $c = new ControlerCreateur();
    $c->setItem($idListe, $idItem);
})->name('modifItem');

//Suppression d'un item
$app->get("/creator/deleteItem/:idListe/:idItem", function ($idListe, $idItem) use ($app) {
    $c = new ControlerCreateur();
    $c->deleteItem($idListe, $idItem);
    //Redirection vers la liste
    $app->redirectTo('viewOneListe', array('idListe' => $idListe));
})->name('deleteItem');


/*****Partie invité*****/
//Création du token invité
$app->get("/invite/formCreateListInvite", function () {
    $v = new \mywishlists\vue\VueCreateur(\mywishlists\vue\VueCreateur::FORM_CREATE_LIST_INVITE, null);
    echo $v->render();
})->name('formCreateListInvite');

$app->post('/invite/createListInvite', function () {
    $c = new ControlerCreateur();
    $c->createListInvite();
})->name('createListInvite');

//Affichage de la liste invité
$app->get("/invite/viewList/:tokenCreation", function ($tokenCreation) use ($app) {
    $c = new ControlerCreateur();
    $c->recupererContenuListe($tokenCreation);
})->name('viewListInvite');

//Création d'un item dans la liste invité
$app->post("/invite/addItem/:tokenCreation", function ($tokenCreation) use ($app) {
    $c = new ControlerCreateur();
    $c->ajouterItemDansListe($tokenCreation);
})->name("addItemInvite");

//Vue d'un item invité
$app->get("/invite/viewItem/:tokenCreateur/:idItem", function ($tokenCreateur, $idItem) use ($app) {
    $c = new ControlerCreateur();
    $c->recupererItem($tokenCreateur, $idItem);
})->name('viewItemInvite');

//Modification d'un item invité
$app->post("/invite/setItem/:tokenCreateur/:idItem", function ($tokenCreateur, $idItem) use ($app) {
    $c = new ControlerCreateur();
    $c->setItem($tokenCreateur, $idItem);
})->name('modifItemInvite');

//Suppression d'item invité
$app->get("/invite/deleteItem/:tokenCreateur/:idItem", function ($tokenCreateur, $idItem) use ($app) {
    $c = new ControlerCreateur();
    $c->deleteItem($tokenCreateur, $idItem);
    $app->redirectTo("viewListInvite", array("tokenCreation" => $tokenCreateur));
})->name("deleteItemInvite");

//Formulaire servant à modifier une liste invité
$app->get("/invite/formSetList/:tokenCreateur", function ($tokenCreateur) use ($app) {
    $c = new ControlerCreateur();
    $c->modificationListe($tokenCreateur);
})->name('formSetListInvite');

//Modification d'une liste en mode invité
$app->post('/invite/setList/:tokenCreateur', function ($tokenCreateur) use ($app) {
    $c = new ControlerCreateur();
    $c->setListe($tokenCreateur);
})->name("modifListeInvite");

//Suppression d'une liste en mode invité
$app->get("/invite/deleteList/:tokenCreateur", function ($tokenCreateur) use ($app) {
    $c = new ControlerCreateur();
    $c->deleteListe($tokenCreateur);
})->name("deleteListInvite");

//Generation de l'URL de partage
$app->get('/invite/shareList/:tokenCreateur', function ($tokenCreateur) use ($app) {
    $c = new ControlerCreateur();
    $c->partagerListe($tokenCreateur);
})->name('shareListInvite');

/*****Partie participant*****/
//Vue d'une liste sans être le créateur
$app->get("/particip/viewList/:token", function ($token) use ($app) {
    $c = new ControlerParticipant();
    $c->afficherListeByToken($token);
})->name('viewOneListeParticip');

//Vue d'un Item côté participant
$app->get("/particip/viewItem/:token/:idItem", function ($token, $idItem) use ($app) {
    $c = new ControlerParticipant();
    $c->afficherItem($token, $idItem);
})->name("viewItem");

//Réservation de l'Item
$app->post("/particip/holdItem/:token/:idItem", function ($token, $idItem) use ($app) {
    $c = new ControlerParticipant();
    $c->reserverItem($token, $idItem);
})->name("holdItem");

$app->run();
