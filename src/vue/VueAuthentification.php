<?php

namespace mywishlists\vue;

/**
 * Class VueAuthentification
 * Regroupe les methodes permettant de gérer la vue de la patie authentifiaction du site
 * @package s3a_zappone_maurice_loeuillet_mardonao\src\vue
 */
class VueAuthentification {
    private $selecteur, $rootUri;
    const LOGIN = 0, SIGN_UP = 1, BAD_LOGIN = 2, BAD_SIGNUP = 3;

    public function __construct($sel) {
        session_start();
        $this->selecteur = $sel;
        $app = \Slim\Slim::getInstance();
        $this->rootUri = $app->request->getRootUri();
    }

    /**
     * Methode utilisant un switch afin de gérer l'affichage de plusieurs pages
     * @return string
     */
    public function render() {
        $content = "";
        $contentAlert = "";
        $app = \Slim\Slim::getInstance();

        switch($this->selecteur) {
            case self::BAD_LOGIN:
                //TODO: A COMPLETER ICI EN CAS D'ERREUR DANS LE FORMULAIRE
                $contentAlert = <<< EOF
    <div class='alert alert-danger' role="alert">
        <strong>Outch ! Quelque chose s'est mal passé :/</strong> L'email et le mot de passe ne correspondent pas !
    </div>
EOF;
            case self::LOGIN:
                $titre = "Connectez-vous - MyWishlist";
                $lienConnexion = $app->urlFor('loginUser');
                $content = <<< EOF
$content
        <div class="jumbotron">
        $contentAlert
        <div class="login-page">
            <div class="form">
                <form class="login-form" action="$lienConnexion" method="POST">
                    <input type="email" placeholder="Entrez votre email" name="email"/>
                    <input type="password" placeholder="Entrez votre mot de passe" name="Password"/>
                    <button type="submit">Se connecter</button>             
                </form>
            </div>
        </div>
        </div>
EOF;
                break;

            case self::BAD_SIGNUP:
                //TODO : A COMPLETER EN CAS D'ERREUR DANS LE FORMULAIRE
                $contentAlert = <<< EOF
    <div class='alert alert-danger'>
        <strong>Il semblerait que vos informations comporte une erreur :/</strong> Vérifier que le mot de passe soit assez complexe et que les deux mots de passes
        tapées sont identiques !
    </div>
EOF;
            case self::SIGN_UP:
                $titre = "Inscrivez-vous - MyWishlist";
                $url = $app->urlFor("createUser");

                $content = <<< EOF
                $content
<div class="jumbotron">        
    <h1 class="display-6">Bienvenue sur myWishlist ! Pouvons-nous vous posez quelques questions ?</h1>        
    <form action="$url" method="post">
      <div class="form-row">
        <div class="form-group col-md-6">
          <label for="inputEmail4">Email</label>
          <input type="email" class="form-control" id="inputEmail4" name="email" placeholder="Email">
        </div>
        <div class="form-group col-md-6">
          <label for="Password">Mot de passe : </label>
          <input type="password" name="Password" class="form-control" id="Password" placeholder="Entrez votre mot de passe">
        </div>
      </div>
      <div class="form-group">
        <label for="confirmPassword">Confirmer le mot de passe : </label>
        <input type="password" name="confirmPassword" class="form-control" id="confirmPassword" placeholder="Retaper votre mot de passe">
      </div>
      <div class="form-row">
        <div class="form-group col-md-6">
          <label for="Nom">Nom : </label>
          <input type="text" class="form-control" name="Nom" id="Nom" placeholder="Entrez votre Nom">
        </div>
        <div class="form-group col-md-6">
          <label for="Prenom">Prenom : </label>
          <input type="text" class="form-control" id="Prenom" name="Prenom" placeholder="Entrez votre prénom">
        </div>
        <div class="form-group col-md-2">
          <label for="PhotoProfil">Choisissez votre photo de profil : </label>
          <input type="file" class="form-control" id="PhotoProfil" name="PhotoProfil" >
        </div>
      </div>
      <div class="form-group">
      <button type="submit" class="btn btn-primary">S'inscrire</button>
      </div>
    </form>
    <br /><br />
    $contentAlert
</div>
EOF;
                break;
        }

        $page = <<<EOF
<!DOCTYPE html>
<html lang='fr'>
    <head>
        <meta charset="utf-8" />
        <title>$titre</title>
        <link rel='stylesheet' href="$this->rootUri/css/bootstrap.min.css" type="text/css" />   
        <link rel='stylesheet' href="$this->rootUri/css/formulaireStyle.css" type="text/css" />  
        <link rel="stylesheet" href="$this->rootUri/css/style.css" type="text/css" />
    </head>
    <body> 
    $content       
    </body>
</html>
EOF;

        return $page;

    }

    /**
     * Methode qui retourne la page d'accueil qui est celle affichée lors de la première visite
     * @return string
     */
    public function accueil() {
        $app = \Slim\Slim::getInstance();
        $rootUri = $app->request->getRootUri();
        $lienConnexion = $app->urlFor('seConnecter');
        $lienInvite = $app->urlFor('formCreateListInvite');
        $lienInscription = $app->urlFor('formCreateUser');
        $titre = "Accueil";

        $page = <<<EOF
<!DOCTYPE html>
<html lang='fr'>
    <head>
        <meta charset="utf-8" />
        <link href="$this->rootUri/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="$rootUri/css/style.css" type="text/css" />
        <meta name="viewport" content="width=device-width, user-scalable=no">
        <title>$titre</title>
    </head>
    <body> 
         <img class="titre" src="image/ph.png" alt="Titre du site wishlist" />
         <div class="bordure">
         <p class="text-center">Bienvenue sur notre site Wishlist !</p>
         <p class="text-center1">Faites consulter votre liste de noël personnalisée par toute votre 
         famille et vos amis et donnez leur ainsi la possibilité d'indiquer le cadeau qu'ils souhaiteraient offrir. 
         Ils sont libres d'acheter le cadeau choisi auprès de l'enseigne que vous aurez indiquée sur votre liste et de 
         le faire livrer à votre adresse, ou de verser une contribution directement sur votre compte si vous avez communiqué 
         votre numéro bancaire, ou de l'acheter dans un magasin de leur propre choix et de vous le remettre en mains propres.
        </p>
         <p class="text-center1">   Fini les cadeaux non souhaités ou reçus en double ! </p>
         </div>
         
         <div class="form">
            <a href="$lienConnexion" class="btn btn-primary btn-success btn-lg btn-block">Se Connecter</a> 
            <a href="$lienInscription" class="btn btn-primary btn-success btn-lg btn-block">S'enregistrer</a>
            <a href="$lienInvite" class="btn btn-primary btn-success btn-lg btn-block">Continuer en tant qu'invité</a>
         </div>
    </body>
</html>
EOF;
        return $page;
    }

    /**
     * Methode qui affiche la page de connexion lorsqu'un utilisateur souhaite se connecter
     * @return string
     */
    public function connexion() {
        $app = \Slim\Slim::getInstance();
        $rootUri = $app->request->getRootUri();
        $titre = "Connexion";
        $lienConnexion = $app->urlFor('loginUser');

        $page = <<<EOF
<!DOCTYPE html>
<html lang='fr'>
    <head>
        <meta charset="utf-8" />
        <link href="assert/css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" href="$rootUri/css/style.css" type="text/css" />
        <meta name="viewport" content="width=device-width">
        <title>$titre</title>
    </head>
    <body>
        <img class="titre" src="$rootUri/image/ph.png" alt="Titre du site wishlist" />
        <div class="login-page">
            <div class="form">
                <form class="login-form" action="$lienConnexion" method="POST">
                    <input type="email" placeholder="Entrez votre email" name="email"/>
                    <input type="password" placeholder="Entrez votre mot de passe" name="Password"/>
                    <button type="submit" >Se connecter</button>             
                </form>
            </div>
        </div>
    </body>
</html>
EOF;
        return $page;
    }
}