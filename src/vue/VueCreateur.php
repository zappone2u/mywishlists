<?php


namespace mywishlists\vue;
use mywishlists\modele\Item as Item;

class VueCreateur {

    const LISTE_CREE = 0, AJOUT_ITEM = 1, AFFICHE_CONTENU_LISTE = 2, AFFICHE_LISTE = 3, SHARE_LIST = 4, AFFICHE_ITEM = 5, MODIF_ITEM = 6, FORM_MODIF_LIST = 7, FORM_CREATE_LIST_INVITE = 8;
    private $selecteur, $liste;
    private $rootUri;

    /**
     * VueCreateur constructor.
     * @param $selecteur
     *      Selectionne la vue qu'on souhaite avoir
     * @param $liste
     *      Informations provenant de la base de données utiles
     */
    public function __construct($selecteur, $liste) {
        $this->selecteur = $selecteur;
        $this->liste = $liste;
        $app = \Slim\Slim::getInstance();
        $this->rootUri = $app->request->getRootUri();
    }

    /**
     * Renvoie la page html a afficher
     * @return string
     *      Code HTML
     */
    public function render() {
        $content = "";
        $contentAlert = "";

        //Instanciation des variables servant pour les routes d'accès à certains fichiers et aux redirections
        $app = \Slim\Slim::getInstance();

        $urlAjoutListe = $app->urlFor('addList');
        $titre = "Vos Listes";

        switch($this->selecteur) {
            case self::LISTE_CREE:
                $contentAlert = '<div class="alert alert-success" role="alert">
                                      La liste a bien été créée !
                                 </div>';
                $app->redirect($app->urlFor('viewListes'));
                break;

            case self::AJOUT_ITEM:
                $contentAlert = '<div class="alert alert-success" role="alert">
                                      L\'item a bien été créé !
                                 </div>';
                $app->redirect($app->urlFor('viewOneListe', array("idListe" => $this->getDernierePartieURL($app))));
                break;

            case self::AFFICHE_LISTE:
                if(!isset($_SESSION['profile']))
                    $app->redirectTo("Accueil");
                $content = $this->afficherListes();
                break;

            case self::AFFICHE_CONTENU_LISTE:
                $content = $this->afficherContenuListe();
                break;

            case self::SHARE_LIST:
                if(isset($_SESSION['profile']))
                    $app->redirect($app->urlFor('viewOneListe', array("idListe" => $this->getDernierePartieURL($app))));
                else
                    $app->redirectTo('viewListInvite', array('tokenCreation' => $this->getDernierePartieURL($app)));

                break;

            case self::AFFICHE_ITEM:
                $titre = "Cadeau - " . $this->liste->nom;
                $content = $this->afficherItemParticulier();
                break;

            case self::MODIF_ITEM:
                $app->redirectTo('viewOneList');
                break;

            case self::FORM_MODIF_LIST:
                $titre = "Modifier Liste - " . $this->liste->titre;
                $content = $this->formModifListe();
                break;

            case self::FORM_CREATE_LIST_INVITE:
                $content = $this->formCreateListInvite();
                break;
            default:
                echo "T'as oublié un case :/";
        }
        if(isset($_SESSION['profile'])) {
            $idUser = $_SESSION['profile']['user_id'];
            $nameParam = "user_id";
            $urlAjoutItem = $app->urlFor('addItem', array('idListe' => $this->getDernierePartieURL($app)));
        }
        else {
            $idUser = $this->getDernierePartieURL($app);
            $nameParam = "tokenCreateur";
            $urlAjoutItem = $app->urlFor('addItemInvite', array('tokenCreation' => $this->getDernierePartieURL($app)));
        }
        $deconnexion = $app->urlFor('logout');
        $page = <<<EOF
<!DOCTYPE html>
<html lang='fr'>
    <head>
        <meta charset="utf-8" />
        <title>$titre</title>
        <meta name="viewport" content="width=device-width, user-scalable=no">
        <link rel='stylesheet' href="$this->rootUri/css/bootstrap.min.css" type="text/css" />   
        <link rel='stylesheet' href="$this->rootUri/css/formulaireStyle.css" type="text/css" />  
        <link rel="stylesheet" href="$this->rootUri/css/style.css" type="text/css" />
    </head>
    <body>        
        <a class='btn btn-danger btnDeco' href="$deconnexion">Se Déconnecter</a>
        $contentAlert
            $content
        <div id="form-item" class="form-popup">
            <form action="$urlAjoutItem" class="form-container" method="post" enctype="multipart/form-data">
                <h1>Ajouter un cadeau</h1>
                <label for="nom"><b>Nom du produit : </b></label>
                <input type="text" id="nom" placeholder="Entrez le nom du produit" name="nom" required>
                
                <label for="descr"><b>Description du produit : </b></label>
                <textarea placeholder="Entrez une description rapide" name="descr" id="descr" cols="33" rows="5" required></textarea>
                
                <label for="imageItem"><strong>Image du produit : </strong></label>
                <input type="file"  name="image" id="imageItem" required />
                <input type="hidden" value="25000" name="MAX_FILE_SIZE" /> 
                
                <label for="tarif"><b>Prix du produit : </b></label>
                <input type="text" placeholder="Entrez le prix du produit" id="tarif" name="tarif" required>
                
                <input type="hidden" name="$nameParam" value="$idUser">
                
                <hr />
                <button class="btn-info col-2-lg addItem" type="submit">Enregistrer le cadeau</button>
                <button class="col-2-lg cancel" onclick="closeFormItem();">Annuler</button>
            </form>
        </div>
        
         <div id="form-list" class="form-popup">
            <form action="$urlAjoutListe" class="form-container" method="post">
                <h1>Créer une nouvelle liste</h1>
                <label for="titre"><b>Titre de la liste : </b></label>
                <input type="text" id="titre" placeholder="Entrez le titre" name="titre" required>
                
                <label for = "https://wishlist.sytes.net/invite/formCreateListInviter='description'"><b>Description de la liste : </b></label>
                <textarea placeholder="Entrez une description rapide" id="description"  name="description" cols="25" rows="5" required></textarea>
                
                <label for="expiration"><b>Date d'expiration de la liste: </b></label>
                <input type="date" id="expiration" name="expiration" required>
                
                <input type="hidden" name="$nameParam" value="$idUser">
                
                <hr />
                <button class="btn-info col-2-lg addList">Enregistrer la liste</button>
                <button class="col-2-lg cancel" onclick="closeFormList();">Annuler</button>
            </form>
        </div>
        <script src="$this->rootUri/js/EventsButton.js"></script>
    </body>
</html>
EOF;
        return $page;
    }

    /**
     * Affiche les détails d'un cadeau
     * @return string
     *      Code HTML correspondant
     */
    public function afficherItemParticulier() {
        //TODO : Penser à implémenter la condition permettant d'attendre la date d'expiration avant d'afficher les messages
        try {
            $value = $this->liste;
            $affichageReservation = "";
            //Condition vérifiant que la date actuelle est bien supérieure à la date d'expiration
            $dateExpiration = $value->liste()->first()->expiration;
            $dateJour = date('d-m-Y');
            if(strtotime($dateJour) >= strtotime($dateExpiration)) {
                if (!is_null($value->pseudoReservation) && !is_null($value->messageReserv)) {
                    $affichageReservation = <<< EOF
            <div class="alert alert-info">
                $value->pseudoReservation vous a offert ce/cette $value->nom et vous a laissé ce message :
                $value->messageReserv
            </div>
EOF;
                }
            }
            $content = <<< EOF
    <div class="card cardItem">
        <img src="$this->rootUri/image/$value->img" alt="Image de $value->nom" class="card-img-top imgDansCard" />
        <div class="card-body">
            <h5 class="card-title">$value->nom</h5>
            <h6 class="card-subtitle">Prix : $value->tarif €</h6>
            <p class="card-text">$value->descr</p>
            $affichageReservation
        </div>
    </div>
EOF;
        }catch(\ErrorException $e) {
            $content = "Aucun élément n'a été trouvé !";
        }
        $content = $content . $this->modifierItem();
        return $content;
    }

    /**
     * Affiche le formulaire de modification d'un item
     * @return string
     *  HTML correspondant
     */
    public function modifierItem() {
        $value = $this->liste;
        $app = \Slim\Slim::getInstance();
        $idItem = $this->getDernierePartieURL($app);
        //L'url de modification d'item diffère en fonction du status de l'utilisateur,
        if(isset($_SESSION['profile'])) {
            $routeModifItem = $app->urlFor('modifItem', array('idListe' => $this->getAvantDernierePartieURL($app), 'idItem' => $idItem));
            $routeSupprimerItem = $app->urlFor("deleteItem", array('idListe' => $this->getAvantDernierePartieURL($app), 'idItem' => $idItem));
        }
        else {
            $routeModifItem = $app->urlFor('modifItemInvite', array('tokenCreateur' => $this->getAvantDernierePartieURL($app), "idItem" => $idItem));
            $routeSupprimerItem = $app->urlFor("deleteItemInvite", array('tokenCreateur' => $this->getAvantDernierePartieURL($app), 'idItem' => $idItem));
        }

        $content = <<< EOF
<br />
        <div class="form-modifItem">
                    <form action="$routeModifItem" method="POST" enctype="multipart/form-data">
                      <div class="form-row align-items-center">
                        <div class="col-auto">
                          <div class="input-group mb-2">
                            <div class="input-group-prepend">
                              <div class="input-group-text">Nom</div>
                          <input type="text" class="form-control mb-2" id="inlineFormInput" name="nom" placeholder="Entrez le nom du produit" value="$value->nom">
                        </div>
                        </div>
                        <div class="col-auto">
                          <div class="input-group mb-2">
                            <div class="input-group-prepend">
                              <div class="input-group-text">Description</div>
                            </div>
                            <input type="text" class="form-control" name="descr" id="inlineFormInputGroup" placeholder="Entrez une Description du produit" value="$value->descr">
                          </div>
                        </div>
                        <div class="col-auto">
                          <div class="input-group mb-2">
                            <div class="input-group-prepend">
                              <div class="input-group-text">Tarif</div>
                            </div>
                            <input type="text" class="form-control" name="tarif" id="inlineFormInputGroup" placeholder="Entrez le tarif du produit" value="$value->tarif">
                          </div>
                        </div>
                        <div class="col-auto">
                          <div class="input-group mb-2">
                            <div class="input-group-prepend">
                              <div class="input-group-text">Photo Produit</div>
                            </div>
                            <input type="file" class="form-control" id="inlineFormInputGroup" name="image" value="$value->descr">
                            <input type="hidden" value="25000" name="MAX_FILE_SIZE" /> 
                          </div>
                        </div>
                        </div>
                        <div class="col-auto">
                          <button type="submit" class="btn btn-primary mb-2">Modifier</button>
                        </div>
                      </div>
                      <input type="hidden" value="$idItem" name="id">
                    </form>
                </div>
                <a href="$routeSupprimerItem"><button type="submit" class="btn btn-danger">Supprimer ce Cadeau</button></a>
                
EOF;
        return $content;
    }

    /**
     * Affiche la liste de toutes les listes d'un utilisateur
     * @return string
     *  HTML Correspondant
     */
    public function afficherListes(){
        $content = " <div class=\"jumbotron\">
           <h1 class = 'display-4'>Vos Listes : </h1>";
        $auteur = $_SESSION['profile']['prenom'];
        $aucuneListe = true;
        $content = $content . "<strong><h6 class='display-5'>Créées par $auteur</h6></strong><br />\n<div class='lead'>\n";
        $app = \Slim\Slim::getInstance();
        foreach($this->liste as $value) {
            if($aucuneListe)
                $aucuneListe = false;
            $url = $app->urlFor('viewOneListe', array('idListe' => $value->no));
            $content = $content . "<p><a href='$url' class='alert alert-info'>" . $value->titre . " : " . $value->description . " Expiration : " . $value->expiration . "\n</a></p><br />";
        }

        if($aucuneListe)
            $content = $content . "<h4 class='display-5'><i>C'est un peu vide par ici. Ajoutez donc vite une nouvelle liste !</i></h4>";
        $content = $content . "</div><hr class='my-4'/>";
        $content = $content . '<button class="btn-info col-2-lg open-button addNewList" onclick="openFormList();">Créer une nouvelle liste</button></div>';
        return $content;
    }

    /**
     * Affiche le contenu d'une liste
     * @return string
     *  Contenu de la liste
     */
    public function afficherContenuListe() {
        $app = \Slim\Slim::getInstance();
        if(isset($_SESSION['profile'])) {
            $urlModifListe = $app->urlFor("formModifListe", array("idListe" => $this->getDernierePartieURL($app)));
            $urlSuppListe = $app->urlFor("deleteListe", array("idListe" => $this->getDernierePartieURL($app)));
        } else {
            $urlModifListe = $app->urlFor("formSetListInvite", array("tokenCreateur" => $this->getDernierePartieURL($app)));
            $urlSuppListe = $app->urlFor("deleteListInvite", array("tokenCreateur" => $this->getDernierePartieURL($app)));
        }
        if(($titreListe = $this->obtenirTitreListe())) {
            //Ici on va tester si le token a été généré ou non pour savoir si l'utilisateur souhaite partager sa liste ou non
            //AFFICHAGE DU LIEN DE PARTAGE
            $token = $this->liste[0]->liste()->first()->token;
            if(!is_null($token) && isset($token)) {
                $urlDePartage = $this->getURLPartage();
                $AffichageLienPartage = <<< EOF
                    <div aria-live="assertive" role="alert" aria-atomic="true" id="notif">
                      <div class="toast">
                        <div class="toast-header">
                          <img src="$this->rootUri/image/validation.png" class="imgValid rounded mr-2" alt="Image d'un bouton de validation">
                          <strong class="mr-auto">Votre liste est terminée ? Partagez là avec vos amis !</strong>
                          <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close" onclick="closeNotif()">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="toast-body">
                          Envoyez leur ceci : $urlDePartage
                        </div>
                      </div>
                    </div>
EOF;
            } else {
                if(isset($_SESSION['profile']))
                    $url = $app->urlFor('shareList', array("idListe" => $this->getDernierePartieURL($app)));
                else
                    $url = $app->urlFor('shareListInvite', array('tokenCreateur' => $this->getDernierePartieURL($app)));

                $AffichageLienPartage = <<< EOF
                    <form action=$url method='get'>
                        <input type="submit" value="Partager la liste" class="btn btn-outline-secondary" />
                    </form>
                    <br />
EOF;
            }
            if(isset($_SESSION['profile']))
                $auteur = $_SESSION['profile']['prenom'];
            else
                $auteur = "un invité";

            $content = <<< EOF
            <div class="jumbotron">
                <h1 class = 'display-4'>$titreListe</h1>
                <h5 class="display-5">Créée par $auteur</h5><br />
                <h2>Voici les cadeaux qui me feraient plaisir : \n</h2>
                <a href="$urlModifListe" class="btn btn-info">Modifier la liste</a>
                <a href="$urlSuppListe" class="btn btn-danger">Supprimer la liste</a>
                <hr class="my-4" />
                <div class='lead'>\n
                $AffichageLienPartage
                <div class = 'list-group'>
EOF;
            foreach ($this->liste as $value) {
                //L'url de vue d'item change en fonction du status de l'utilisateur : invité ou connecté
                if(isset($_SESSION['profile'])) {
                    $urlVueItem = $app->urlFor('ViewItemCrea', array("idListe" => $value->liste()->first()->no, "idItem" => $value->id));
                    $urlSuppItem = $app->urlFor('deleteItem', array("idListe" => $value->liste()->first()->no, "idItem" => $value->id));
                }
                else {
                    $urlVueItem = $app->urlFor('viewItemInvite', array("tokenCreateur" => $this->getDernierePartieURL($app), "idItem" => $value->id));
                    $urlSuppItem = $app->urlFor('deleteItemInvite', array("tokenCreateur" => $value->liste()->first()->tokenCreateur, "idItem" => $value->id));
                }
                $badge = "";
                if($this->estDejaReserve($value->id))
                    $badge = '<span class="badge badge-success" >Réservé</span>';

                $content = <<< EOF
                $content                
                <a href="$urlVueItem" class="list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-between">
                     <h5 class="mb-1">$value->nom $badge</h5>
                    <small>$value->expiration</small>
                </div>
        <p class="mb-1">$value->descr</p>
        <small>$value->tarif €</small>
        <a href="$urlSuppItem"><button class="btn btn-danger">Supprimer ce cadeau</button></a>

        </a>
    </div>
EOF;
            }
        } else {
            $content = "<div class='jumbotron'>";
            $content = $content . "<h1 class='display-4'>Oups, Cette Liste Ne Contient Aucun élément :/</h1>\n";
            $content = $content . "<h4>Ajoutez-en un dès maintenant !</h4>";
        }
        $content = <<< EOF
        $content
        </div>\n</p><hr class='my-4'/>
              
        <button class="btn-info col-2-lg open-button" type="submit" onclick="openFormItem();">Ajouter un cadeau</button></div>
EOF;
        return $content;
    }

    /**
     * Affiche le formulaire d'une liste
     * @return string
     *      HTML Correspondant
     */
    public function formModifListe() {
        $value = $this->liste;
        $app = \Slim\Slim::getInstance();

        if(isset($_SESSION['profile']))
            $urlModifListe = $app->urlFor("modifListe", array('idListe' => $this->getDernierePartieURL($app)));
        else
            $urlModifListe = $app->urlFor("modifListeInvite", array('tokenCreateur' => $this->getDernierePartieURL($app)));

        $content = <<< EOF
    <div class="jumbotron">
        <h1 class="display-4">Quelque chose ne convient pas ? Aucun soucis !</h1>
        <form method="POST" action="$urlModifListe">
          <div class="form-group row">
            <label for="titre" class="col-sm-2 col-form-label">Titre de la liste : </label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="titre" name="titre" placeholder="Entrez le titre de la liste" value="$value->titre">
            </div>
          </div>
          <div class="form-group row">
            <label for="description" class="col-sm-2 col-form-label">Description : </label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="description" name="description" placeholder="Entrez une description rapide de la liste" value="$value->description">
            </div>
          </div>
          <div class="form-group row">
            <label for="expiration" class="col-sm-2 col-form-label">Date d'expiration de la liste : </label>
            <div class="col-sm-10">
              <input type="date" class="form-control" id="expiration" placeholder="Entrez la date d'expiration de la liste" name="expiration" value="$value->expiration">
            </div>
          </div>
          <div class="form-group row">
            <div class="col-sm-10">
              <button type="submit" class="btn btn-primary">Confirmer la modification</button>
            </div>
          </div>
        </form>
    </div>
EOF;
        return $content;
    }

    /**
     * Formulaire de création d'une liste invité
     * @return string
     *      HTML Correspondant
     */
    public function formCreateListInvite() {
        $app = \Slim\Slim::getInstance();
        $urlCreationListe = $app->urlFor('createListInvite');
        $content = <<< EOF
    <div class="jumbotron">
        <h1 class="display-4">Un évènement ? Un anniversaire ? Wishlist est là pour vous !</h1>
        <form method="POST" action="$urlCreationListe">
          <div class="form-group row">
            <label for="titre" class="col-sm-2 col-form-label">Titre de la liste : </label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="titre" name="titre" placeholder="Entrez le titre de la liste">
            </div>
          </div>
          <div class="form-group row">
            <label for="description" class="col-sm-2 col-form-label">Description : </label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="description" name="description" placeholder="Entrez une description rapide de la liste">
            </div>
          </div>
          <div class="form-group row">
            <label for="expiration" class="col-sm-2 col-form-label">Date d'expiration de la liste : </label>
            <div class="col-sm-10">
              <input type="date" class="form-control" id="expiration" name="expiration">
            </div>
          </div>
          <div class="form-group row">
            <div class="col-sm-10">
              <button type="submit" class="btn btn-primary">Créer ma liste</button>
            </div>
          </div>
        </form>
    </div>
EOF;
        return $content;
    }

    /**
     * Renvoie le titre de la liste
     * @return bool
     *  faux si le titre n'est pas trouvé
     */
    public function obtenirTitreListe() {
        foreach($this->liste as $value) {
            $firstItem = Item::where('id', $value->id)->first()->liste()->first();
            break;
        }
        if(isset($firstItem))
            $titreListe = $firstItem->titre;
        else
            return false;
        return $titreListe;
    }

    /*****************************************FONCTIONS INTERNES UTILES *************************************************************/

    /**
     * Renvoie la dernière partie de l'URL
     * @param $app
     *         Instance de Slim
     * @return mixed
     *         Valeur dans l'URL
     */
    private function getDernierePartieURL($app) {
        $varDump = $app->request->getResourceUri();
        $varDump = explode('/', $varDump);
        return filter_var($varDump[count($varDump) - 1], FILTER_SANITIZE_SPECIAL_CHARS);
    }

    /**
     * Renvoie l'avant dernière partie de l'URL
     * @param $app
     *      Instance de Slim
     * @return mixed
     *      Valeur de l'URL
     */
    private function getAvantDernierePartieURL($app) {
        $varDump = $app->request->getResourceUri();
        $varDump = explode('/', $varDump);
        return filter_var($varDump[count($varDump) - 2], FILTER_SANITIZE_SPECIAL_CHARS);
    }

    /**
     * Génère le lien de partage d'une liste
     * @return string
     *      Lien de partage de la liste
     */
    private function getURLPartage() {
        $app = \Slim\Slim::getInstance();
        return $app->request->getHost() . $app->urlFor('viewOneListeParticip', array('token' => $this->liste[0]->liste()->first()->token));
    }

    /**
     * Verifie si un item est deja reserve ou non
     * @param $idItem
     *      id de l'item a verifier
     * @return bool
     *      vrai si l'item est deja reserve
     */
    private function estDejaReserve($idItem) {
        return !is_null(Item::where('id', $idItem)->first()->pseudoReservation);
    }
}