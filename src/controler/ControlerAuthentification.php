<?php

namespace mywishlists\controler;
use mywishlists\Erreur\AuthException as AuthException;
use mywishlists\modele\Compte as Compte;

/**
 * Class ControlerAuthentification
 * Regroupe les methodes de gestion des comptes utilisateurs
 * @package s3a_zappone_maurice_loeuillet_mardonao\src\controler
 */
class ControlerAuthentification extends Controler
{

    const ROLE_ADMIN = 1, ROLE_CREATEUR_NORMAL = 0;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Cree un compte pour un utilisateur
     */
    public static function createUser()
    {
        if (isset($_POST['Password'])) {
            if ($_POST['Password'] === $_POST['confirmPassword']) {
                $pass = $_POST['Password'];
                //Définition des regex pour check le mot de passe
                $majuscule = preg_match("@[A-Z]@", $pass);
                $minuscule = preg_match("@[a-z]@", $pass);
                $numero = preg_match("@[0-9]@", $pass);
                $specialCharacter = preg_match("/\W/", $pass);
                echo "\n<br /><br />longueur : " . strlen($pass) . " maj : " . $majuscule . " minuscule : " . $minuscule . " num : " . $numero . " specialCharacter : " . $specialCharacter;

                //On vérifie que le mot de passe a bien toutes les conditions requises
                if (strlen($pass) > 8 && $majuscule && $minuscule && $numero && $specialCharacter) {
                    $pass_hache = password_hash($_POST['Password'], PASSWORD_DEFAULT);
                    $insert = new Compte();
                    $insert->Nom = filter_var($_POST['Nom'], FILTER_SANITIZE_STRING);
                    $insert->Prenom = filter_var($_POST['Prenom'], FILTER_SANITIZE_STRING);
                    $insert->Password = $pass_hache;
                    $insert->email = filter_var($_POST['email'], FILTER_VALIDATE_EMAIL);
                    try {
                        if (!$insert->email)
                            throw new \ErrorException();
                    } catch (\ErrorException $excep) {
                        echo("Erreur lors de la creation de votre compte : l'Email fournit est incorrect");
                    }
                    $insert->save();
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Connecte un utilisateur a son compte
     * @param $email corrspond a l'email associe au compte
     * @param $passworr corrspond au mot de passe associe au compte
     * @return boolean retourne Vrai si la connexion a reussi, faux sinon
     */
    public static function authenticate($email, $password)
    {
        if (!isset($_SESSION['profile'])) {
            $hashPassword = Compte::select('Password')
                ->where('email', '=', $email)
                ->first();
            if (!is_null($hashPassword)) {
                if (password_verify($password, $hashPassword->Password)) {
                    $user_id = Compte::select('user_id')->where('email', $email)->first();
                    self::loadProfiles($user_id->user_id);
                    return true;
                }
            }
            return false;
        } else {
            return true;
        }
    }

    /**
     * Charge les éléments de l'utilisateur connecté dans une variable de sesssion
     * @param $uid
     *     ID de l'utilisateur connecté
     */
    private static function loadProfiles($uid)
    {
        try {
            session_destroy();
        } catch (\Exception $e) {
            echo "La session n'a pas été détruite car elle n'existait pas !";
        }
        session_start();
        $infos = Compte::select('Prenom', 'Role')->where('user_id', $uid)->first();
        $_SESSION['profile']['user_id'] = $uid;
        $_SESSION['profile']['prenom'] = $infos->Prenom;
        $_SESSION['profile']['role'] = $infos->Role;
    }

    /**
     * Deconnecte un utilisateur deja connecte
     */
    public static function logout()
    {
        session_start();
        session_destroy();
        $app = \Slim\Slim::getInstance();
        //On redirige vers la page d'accueil du site web
        $app->redirectTo("Accueil");
    }

    /**
     * Vérifie si l'utilisateur souhaitant effectuer une action a les droits suffisants
     * @param $required
     *      Niveau de privilèges requis
     * @return boolean
     *      Vrai si l'utilisateur a les droits, faux sinon
     */
    public static function checkAccessRight($required)
    {
        if (isset($_SESSION['profile'])) {
            try {
                if ($_SESSION['profile']['role'] < $required) {
                    throw new AuthException("Permission Denied");
                }
            } catch (AuthException $e) {
                $mess = $e->getMessage();
                echo "<script>alert($mess);</script>";
                return false;
            }
            return true;
        }
    }

    /**
     * Permet a un utilisateur connecte de modifier les informations de son compte
     * @param $ids
     *  Information du compte a modifier
     */
    public function modifierInformationsCompte($ids)
    {
        if (isset($_SESSION['profile']['user_id'])) {
            //TODO : A COMPLETER
        }
    }

    /**
     * Permet a un utilisateur authentifie de supprimer son compte
     */
    public function supprimerCompte()
    {
        if (isset($_SESSION['profile']['user_id'])) {
            //TODO : A COMPLETER
        }
    }

}