<?php

namespace mywishlists\controler;

/**
 * Class Controler
 * @package s3a_zappone_maurice_loeuillet_mardonao\src\controler
 *  Permet l'activation de session sur l'ensemble des pages qui vont être chargées
 */
abstract class Controler {

    /**
     * Controler constructor.
     *  Lance session_start();
     */
    public function __construct() {
        session_start();
    }
}