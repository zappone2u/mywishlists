<?php

namespace mywishlists\controler;


use mywishlists\modele\Item as Item;
use mywishlists\modele\Liste as Liste;
use mywishlists\vue\VueParticipant as VueParticipant;

/**
 * Regroupe les methodes utiles pour la personne souhaitant reserver ou interagir avec la liste de c
 * adeaux
 * @package s3a_zappone_maurice_loeuillet_mardonao\src\controler
 */
class ControlerParticipant extends Controler {

    /**
     * Affiche les cadeaux de la liste
     * @param $token
     *  token d'autorisation d'acces a une liste
     */
    public function afficherListeByToken($tokenCreateur) {
        $idListe = Liste::where("token", $tokenCreateur)->first()->no;
        $content = Item::where("liste_id", $idListe)->get();
        $_SESSION['token'] = $tokenCreateur;

        $vp = new VueParticipant($content, VueParticipant::ITEMS_VIEW);
        echo $vp->render();
    }

    /**
     * Permet a un utilisateur non connecte mais ayant acces au token de reserver un item
     * @param $idItem
     * @param $nom
     */
    public function reserverItem($tokenCreateur, $idItem) {
        if(isset($_POST['pseudoReservation'])) {
            $idListe = Liste::where("token", "=", $tokenCreateur);

            $itemRecherche = Item::where("id", $idItem)->first();
            $itemRecherche->pseudoReservation = filter_var($_POST['pseudoReservation'], FILTER_SANITIZE_STRING);
            $itemRecherche->messageReserv = filter_var($_POST['message'], FILTER_SANITIZE_STRING);
            $itemRecherche->save();

            $app = \Slim\Slim::getInstance();
            $app->redirect($app->urlFor('viewItem', array("token" => $tokenCreateur, "idItem" => $idItem)));
        }
    }

    /**
     * Obtiens la liste des items d'une liste
     * @param $token
     *      Token d'accès
     * @param $idItem
     *      Item à chercher
     */
    public function afficherItem($tokenCreateur, $idItem)
    {
        $idList = Liste::where('token', '=', $tokenCreateur)->first()->no;
        $item = Item::where("liste_id", '=', $idList)->where('id', '=', $idItem)->first();

        $v = new VueParticipant($item, VueParticipant::ITEM_VIEW);
        echo $v->render();
    }
}