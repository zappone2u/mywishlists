<?php

namespace mywishlists\controler;

use mywishlists\modele\Compte as Compte ;
use mywishlists\modele\Item as Item ;
use mywishlists\modele\Liste as Liste ;


class ControlerAffichage{

    public function getListes(){
        $reqListes = Liste::select('no','titre','description')->where('publique','=',true);
        $listes = $reqListes->get();
        $v = new \mywishlists\vue\VueAffichage($listes->toArray());
        $v->render(1);
    }

    public function getListe($no){
        $liste = \mywishlists\modele\Liste::find($no);
        $v = new \mywishlists\vue\VueCreation([$liste]);
        $v->render(1);
    }

    public function getItemsListe($no){
        $liste = Liste::find($no);
        $utilisateur=null;
        if(isset($_SESSION["login"])){
            $utilisateur = Compte::where("login","=",$_SESSION["login"])->first();
        }
        $v = new \mywishlists\vue\VueAffichage([$liste, $utilisateur]);
        $v->render(2);
    }

    public function getItem($idItem){
        $item = Item::find($idItem);
        $v = new \mywishlists\vue\VueAffichage([$item]);
        $v->render(3);
    }

    public function partageListeToken($tokenCreateur){
        $utilisateur=null;
        if(isset($_SESSION["login"])){
            $utilisateur = Utilisateur::where("login","=",$_SESSION["login"])->first();
        }
        $liste=Liste::where("token","=",$tokenCreateur)->first();
        $v = new \mywishlists\vue\VueAffichage([$liste,$utilisateur]);
        $v->render(2);
    }

    public function getAccueil(){
        $v = new \mywishlists\vue\VueAffichage();
        $v->render(4);
    }

}