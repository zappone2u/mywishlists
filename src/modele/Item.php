<?php

namespace mywishlists\modele;
Use \Illuminate\Database\Eloquent\Model as Model;

class Item extends Model{
    protected $table = 'item';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function liste(){
        return $this->belongsTo('mywishlists\modele\liste','liste_id');
    }
}