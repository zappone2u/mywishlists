<?php

namespace mywishlists\modele;
Use \Illuminate\Database\Eloquent\Model as Model;

class Compte extends Model{
    protected $table = 'compte';
    protected $primaryKey = 'user_id';
    public $timestamps = false;
}