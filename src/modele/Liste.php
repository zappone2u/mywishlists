<?php

namespace mywishlists\modele;
Use \Illuminate\Database\Eloquent\Model as Model;

class Liste extends Model{
    protected $table = 'liste';
    protected $primaryKey = 'no';
    public $timestamps = false;

    public function liste(){
        return $this->hasmany('mywishlists\modele\item','liste_id');
    }
}