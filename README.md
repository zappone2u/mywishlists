#Projet Mywishlists

Ce ReadMe vous permet de vous connecter en tant qu'administrateur sur ce projet. 

##### Utiliser notre site web 100% fonctionnel
Nous l'avons hébrerger sur notre propre Rasberry que nous avons configurer.

##### Instructions d'installation (sur son serveur web) :
* Clôner le dépôt dans votre dossier www de votre serveur (ici nous avons WampServer)
* Ajouter un fichier `.htaccess` à la racine du projet autorisant la réécriture d'url sur le fichier index.php et n'autorisant pas l'accès aux fichiers sources
 
 Exemple :

        RewriteEngine On
        #
        # RewriteBase indispensable sur webetu :
        
        # RewriteBase /www/username0/mywishlist
        
        
        #
        # Pour interdire l'accès aux répertoires contenant du code
        RewriteRule ^sql(/.*|)$ - [NC,F]
        RewriteRule ^src(/.*|)$ - [NC,F]
        RewriteRule ^vendor(/.*|)$ - [NC,F]
        
        #
        # réécriture pour slim
        
        RewriteCond %{REQUEST_FILENAME} !-d
        RewriteCond %{REQUEST_FILENAME} !-f
        RewriteRule ^ index.php [QSA,L]
        
* Puis ajouter un fichier conf.ini contenant les identifiants qui vous ont été fournis et placer le fichier dans mywishlists/src/conf

Exemple : 

        driver=mysql
        username=
        password=
        host=
        database=mydblist
        charset=utf8
        collation=utf8_unicode_ci


* Ensuite lancer `composer install` sur git bash à la racine du projet afin d'installer toutes les dépendances.
* Enfin vous pouvez lancer sur votre navigateur et avoir accès à notre site internet 