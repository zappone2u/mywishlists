function openFormItem() {
    document.getElementById("form-item").style.display = "block";
}

function closeFormItem() {
    document.getElementById("form-item").style.display = "none";
}

function openFormList() {
    document.getElementById("form-list").style.display = "block";
}

function closeFormList() {
    document.getElementById("form-list").style.display = "none";
}

function closeNotif() {
    document.getElementById("notif").style.display = "none";
}


function waitAMoment() {
    setTimeout(function() {}, 5000);
}
